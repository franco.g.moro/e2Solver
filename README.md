# E2Solver
To Run:
Go to src/test/java/SolverTest.java
Right click on the test method and run.
The program should stop after 2000 iterations. You can modify this number under the variable MAX_ITERATIONS.

You can remove standard output prints by replacing the boolean with false.
```
Solver solver = new Solver(true);
```
