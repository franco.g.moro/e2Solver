package AnalysisTool;

import core.Piece;
import util.E2SetReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Analysis {

    public void getCategoryCount(){
        List<Piece> pieces = new ArrayList<>();
        try {
            pieces.addAll(E2SetReader.getCSV("pieces.csv"));
        }
        catch (IOException e){
            System.out.println(e);
        }

        HashMap<Integer,Integer> edgeTypeMap = new HashMap(pieces.size());
        int totalCount = 0;
        int pieceCount = 0;
        for(Piece piece : pieces){
            pieceCount++;
            for(int edge : piece.getEdges() ){
                totalCount++;
                edgeTypeMap.putIfAbsent(edge,0);
                Integer updatedCount = edgeTypeMap.get(edge)+1;
                edgeTypeMap.replace(edge,updatedCount );
            }
        }

        int sum = 0;
        int denominator =0;
        for(int i = 0 ; i < edgeTypeMap.size() ; i++){
            int count = edgeTypeMap.get(i);
            System.out.print((char)(65+ i) +": "+count+"\t");
            if((i+1)%6==0){
                System.out.println("");
            }
            denominator++;
            sum += count;
        }
        //
        System.out.println("");
        System.out.println("Average Degree: ");
        System.out.println(sum/denominator);
        HashMap<Piece,Integer> duplicateMap = new HashMap<>();
        for(int i = 0 ; i < pieces.size() ; i++){
            Piece targetPiece = pieces.get(i);
            int duplicateCount  = 0;
            if(duplicateMap.containsKey(targetPiece)){
                continue;
            }
            for(int j = 0 ;  j < pieces.size() ; j++){
                if(i!=j && pieces.get(j).equals(targetPiece)){
                    duplicateCount++;
                }
            }
            duplicateMap.put(targetPiece,duplicateCount);

        }
        System.out.println("Number of types: " +edgeTypeMap.size());
        System.out.println("Total edges: " + totalCount);
        System.out.println("Total pieces: " + pieceCount);
        duplicateMap.forEach(( p, count) -> {
            System.out.println(p +" has "+count + " duplicates.");
        });

    }


}
