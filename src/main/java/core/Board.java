package core;

public class Board {
    private Piece[][] board;
    private int size;

    public Board(int nPieces) {
        this.size = (int) Math.sqrt(nPieces);
        board = new Piece[nPieces][nPieces];
    }

    public void put(int i, int j, Piece piece) {
        board[i][j] = piece;
    }

    public Piece get(int i, int j) {
        return board[i][j];
    }

    public int getSize() {
        return size;
    }

    public String toString() {
        StringBuilder bob = new StringBuilder("BOARD STATE: " + "\n");
        String formatTop = "|  %-2d %-2s|";
        String formatMiddle = "| %-2d %-2d |";

        for (int i = 0; i < size; i++) {
            if(this.board[i][0]==null){
                break;
            }
            String top = "";
            String middle = "";
            String bottom = "";
            for (int j = 0; j < size; j++) {
                Piece piece = board[i][j];
                if (piece == null){
                    top += String.format(formatTop,-1, "  ");
                    middle += String.format(formatMiddle,-1,-1) ;
                    bottom += String.format(formatTop,-1,"  ");
                }
                else{
                    top += String.format(formatTop,piece.getTop(),"  ");
                    middle += String.format(formatMiddle,piece.getLeft(),piece.getRight()) ;
                    bottom += String.format(formatTop,piece.getBottom(),"  ");
                }
            }
            String bottomSeparator = "";
            for(int k=0;k<bottom.length();k++){
                bottomSeparator+="_";
            }
            bob.append(top)
                    .append("\n")
                    .append(middle)
                    .append("\n")
                    .append(bottom)
                    .append("\n")
                    .append(bottomSeparator)
                    .append("\n");
        }
        return bob.toString();
    }
}
