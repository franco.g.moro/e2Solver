package core;

public class Index {
    public int i;
    public int j;
    int rowLength;

    public Index(int i, int j, int rowLenght) {
        this.i = i;
        this.j = j;
        this.rowLength = rowLenght;
    }

    public Index increment() {
        if (j == rowLength - 1) {
            this.i++;
            this.j = 0;
        } else {
            j++;
        }
        return this;
    }

    public Index decrement() {
        if (j == 0) {
            i--;
            j = rowLength - 1;
        } else {
            j = j - 1;
        }
        return this;
    }

    public Boolean indexReachedEnd() {
        return (i == rowLength && j == rowLength);
    }

    public int getI() {
        return i;
    }


    public int getJ() {
        return j;
    }

}
