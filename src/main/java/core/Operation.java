package core;

public class Operation implements Comparable {
    int scoreChange;


    Index index1;
    Index index2;



    public Operation(Index index1) {
        this.index1 = index1;
    }

    public Operation(Index index1, Index index2) {
        this.index1 = index1;
        this.index2 = index2;
    }

    public int getScoreChange() {
        return scoreChange;
    }

    public void setScoreChange(int scoreChange) {
        this.scoreChange = scoreChange;
    }

    public boolean isSwap(){
        return (this.index2 != null);
    }

    public Index getIndex1() {
        return index1;
    }

    public Index getIndex2() {
        return index2;
    }

    @Override
    public int compareTo(Object o) {
        return this.scoreChange - ((Operation) o).scoreChange;
    }
}

