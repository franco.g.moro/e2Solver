package core;

import java.util.Arrays;

public class Piece {
    // Top,Right,Bottom,Left
    Integer[] edges;

    public Piece(String csvLine) {
        String[] numStrings = csvLine.split(",");
        this.edges = new Integer[numStrings.length];
        for (int i = 0; i < numStrings.length; i++) {
            this.edges[i] = Integer.valueOf(numStrings[i]);
        }
    }

    public Integer[] getEdges() {
        return edges;
    }

    public int getTop() {
        return edges[0];
    }

    public int getRight() {
        return edges[1];
    }

    public int getBottom() {
        return edges[2];
    }

    public int getLeft() {
        return edges[3];
    }


    /**
     * Rotates the piece clockwise.
     * @return
     */
    public Piece rotate() {
        int temp = edges[3];
        edges[3] = edges[2];
        edges[2] = edges[1];
        edges[1] = edges[0];
        edges[0] = temp;
        return this;
    }


    /**
     * Right is optional, it can be -1
     *
     * @param left
     * @param top
     * @param right
     * @return
     */
    public Boolean hasEdges(int top, int right, int bottom, int left) {
        boolean skipBottom = (bottom < 0);
        boolean skipRight = (right < 0);
        for (int i = 0; i < edges.length; i++) {
            if (left == this.edges[i] && top == this.edges[(i + 1) % 4]) {
                if (skipRight) {
                    return true;
                } else {
                    if (right == this.edges[(i + 2) % 4]) {
                        if(skipBottom){
                            return true;
                        }
                        else {
                            return (bottom==this.edges[(i+3) % 4]);
                        }
                    }
                }
            }
        }
        return false;
    }


    /**
     * Returns the edge type number of the found symetric corner. If none, -1.
     *
     * @return
     */
    public int getSymetricCorner() {
        for (int i = 0; i < edges.length; i++) {
            if (this.edges[i] == this.edges[(i + 1) % 4]) {
                return this.edges[i];
            }
        }
        return -1;
    }


    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        Piece piece = (Piece) o;
        // Since we rotate the pieces, the edges might be shifted
        // 4
        for(int i= 0;i<edges.length;i++){
            if(edges[i] == piece.edges[0] && edges[(i+1) %4] == piece.edges[1] && edges[(i+2) %4] == piece.edges[2] && edges[(i+3) %4] == piece.edges[3]){
                return true;
            }
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(edges);
    }
}
