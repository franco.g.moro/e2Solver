package core;

import util.E2SetReader;

import java.io.IOException;
import java.util.*;

public class Solver {
    public final long MAX_ITERATIONS = 2000;
    List<Piece> pieces;
    // List faster than set when N < 5 buuuuut we need the functionality of lists.
    HashMap<Integer, List<Piece>> piecesByEdgeType;
    Board board;
    // Pieces at the four corners of the board.
    Stack<Tuple<Piece, Integer>> usedPieces;
    boolean stdOut;
    Index currentIndex;
    int nth;

    long iterations;

    public Solver(boolean stdOut) {
        this.stdOut = stdOut;
        pieces = new ArrayList<>();
        iterations = 0;
        piecesByEdgeType = new HashMap<Integer, List<Piece>>();
        HashMap<Integer, List<Piece>> symetricCornerPieces = new HashMap<>();
        usedPieces = new Stack<>();

        nth = 0;
        try {
            pieces.addAll(E2SetReader.getCSV("pieces.csv"));
        } catch (IOException e) {
            System.out.println(e);
        }
        board = new Board(pieces.size());
        currentIndex = new Index(0, 0, board.getSize());
        for (Piece p : pieces) {
            // Find the pieces with symetric corners.
            List<Piece> set;
            int cornerType = p.getSymetricCorner();
            if (cornerType >= 0) {
                set = symetricCornerPieces.get(cornerType);
                if (set == null) {
                    symetricCornerPieces.put(cornerType, new ArrayList<>());
                    set = symetricCornerPieces.get(cornerType);
                }
                if (!set.contains(p)) {
                    set.add(p);
                }
            }

            for (int edge : p.edges) {

                // Place in our map
                set = piecesByEdgeType.get(edge);
                if (set == null) {
                    piecesByEdgeType.put(edge, new ArrayList<>());
                    set = piecesByEdgeType.get(edge);
                }
                if (!set.contains(p)) {
                    set.add(p);
                }

            }
        }

        // Remove every corner piece from borders.
        for (List<Piece> set : piecesByEdgeType.values()) {
            for (Piece p : symetricCornerPieces.get(0)) {
                set.remove(p);
            }
        }
        piecesByEdgeType.put(50, symetricCornerPieces.get(0));

    }

    public Board solve() {
        int boardSize = board.getSize();
        List<Piece> corner = new ArrayList<>();
        corner.addAll(this.piecesByEdgeType.get(50));
        // n^2
        while (!currentIndex.indexReachedEnd() || iterations == MAX_ITERATIONS) {
            iterations++;
            Piece candidate = null;
            if (currentIndex.getI() == 0 || currentIndex.getI() == boardSize - 1) {
                if (currentIndex.getJ() == 0 && currentIndex.getI() == 0) {
                    // First piece.
                    candidate = corner.get(new Random().nextInt(corner.size()));
                    while (!(candidate.getLeft() == 0 && candidate.getTop() == 0)) {
                        candidate.rotate();
                    }
                }
                // Pieces on top and bottom row border.
                // TODO:: do bottom row as well pls.
                else {
                    // I know I'm supposed to use a getter method but I see this as a flaw in java.
                    Piece previous = this.board.get(currentIndex.i, currentIndex.j - 1);
                    if (this.currentIndex.getJ() == boardSize - 1) {
                        candidate = findNthCandidate(0, 0, previous.getRight(), -1, nth);
                    } else {
                        candidate = findNthCandidate(0, -1, previous.getRight(), -1, nth);
                    }
                    if (candidate == null) {
                        backTrack();
                    } else {
                        while (!(previous.getRight() == candidate.getLeft() && candidate.getTop() == 0)) {
                            candidate.rotate();
                        }
                    }
                }
            } else if (currentIndex.getI() > 0 && currentIndex.getI() < (boardSize - 1)) {
                Piece above = board.get(currentIndex.i - 1, currentIndex.j);

                if (currentIndex.j == 0) {
                    // First piece of a row between the top and bottom ones.
                    candidate = findNthCandidate(above.getBottom(), -1, 0, -1, nth);
                    if (candidate == null) {
                        backTrack();
                    } else {
                        while (!(0 == candidate.getLeft() && candidate.getTop() == above.getBottom())) {
                            candidate.rotate();
                        }
                    }
                } else {
                    Piece previousInRow = this.board.get(currentIndex.i, currentIndex.j - 1);
                    if (currentIndex.getJ() == (boardSize - 1)) {
                        // Last piece of a row between the top and bottom ones.
                        candidate = findNthCandidate(above.getBottom(), 0, previousInRow.getRight(), -1, nth);
                    } else {
                        candidate = findNthCandidate(above.getBottom(), -1, previousInRow.getRight(), -1, nth);
                    }
                    if (candidate == null) {
                        backTrack();
                    } else {
                        while (!(previousInRow.getRight() == candidate.getLeft() && candidate.getTop() == above.getBottom())) {
                            candidate.rotate();
                        }
                    }
                }
            }
            if (candidate != null) {
                this.usedPieces.push(new Tuple<>(candidate, this.nth));
                this.board.put(currentIndex.i, currentIndex.j, candidate);
                currentIndex.increment();
                this.nth = 0;
            }

        }
        return this.board;
    }

    private void backTrack() {
        // Only for graphical purposes.
        this.board.put(this.currentIndex.i, this.currentIndex.j, null);

        this.currentIndex.decrement();
        Tuple<Piece, Integer> lastTry = usedPieces.pop();
        this.nth = lastTry.y + 1;
        if(stdOut){
            System.out.println("Failed, nth: " + this.nth);
            System.out.println(this.board);
        }
    }


    /**
     * Finds the nth candidate that would fit within the input parameters.
     * @param top
     * @param right
     * @param left
     * @param bottom
     * @param nth
     * @return
     */
    private Piece findNthCandidate(int top, int right, int left, int bottom, int nth) {
        List<Piece> candidates = new ArrayList<>();
        List<Piece> piecesWithSameEdgeType = null;
        // Let's say we don't care about corner cases because they are handled above.
        if (top == 0 || left == 0 || right == 0 || bottom == 0) {
            // Corner
            if ((left == 0 && top == 0) || (top == 0 && right == 0) || (right == 0 && bottom == 0) || (bottom == 0 && left == 0)) {

                // Constant
                for (Piece p : this.piecesByEdgeType.get(50)) {
                    if (p.hasEdges(top, right, bottom, left) && !hasBeenUsed(p)) {
                        candidates.add(p);
                    }
                }
            }
            // Border not corner
            else {
                piecesWithSameEdgeType = this.piecesByEdgeType.get(0);
            }
        } else {
            piecesWithSameEdgeType = this.piecesByEdgeType.get(left);
        }
        if (piecesWithSameEdgeType != null) {
            // size of set times 4.
            for (Piece p : piecesWithSameEdgeType) {
                if (p.hasEdges(top, right, bottom, left) && !hasBeenUsed(p)) {
                    candidates.add(p);
                }
            }
        }
        if (candidates.size() < 1) {
            // No candidates have been found, need to backtrack();
            return null;
        }

        if (candidates.size() - 1 < nth) {
            candidates = null;
            // No more candidates, need to change
            backTrack();
            return null;
        }


        return candidates.get(nth);
        //  make sure it's not border piece if not supposed to be.
    }

    private boolean hasBeenUsed(Piece p) {
        // this takes n where n is the size of used pieces. Gets linearly longer on each position.
        for (Tuple<Piece, Integer> t : usedPieces) {
            if (t.x.equals(p)) {
                return true;
            }
        }
        return false;
    }


    public Board getBoard() {
        return board;
    }
}
