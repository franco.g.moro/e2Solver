package util;

import core.Piece;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class E2SetReader {

    public static List<Piece> getCSV(String filePath) throws IOException {

        File file = new File(filePath);
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = "";
        List<Piece> pieces = new ArrayList<>();
        reader.readLine();
        while ((line = reader.readLine()) != null) {
            pieces.add(new Piece(line));
        }
        return pieces;
    }
}
