import core.Piece;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PieceTest {
    @Test
    public void pieceTest(){
        Piece piece = new Piece("0,1,2,3");
        Piece piece1 = new Piece("3,0,1,2");
        Assertions.assertEquals(piece,piece1);
    }
    @Test
    public void pieceTest2(){
        Piece piece = new Piece("3,2,0,0");
        Piece piece1 = new Piece("0,2,3,0");
        Assertions.assertNotEquals(piece,piece1);
    }
}
